## Kotori 4.0.0

- Port code base to kotlin
- Use android ktx location
- Update application logic
- Night theme is darker
- Change app theme from preferences
- Add spanish traslation
- Display current speed in notification instead of max speed
- Add Exit option
- Portrait support
- Change status and navigation bar colors
- Remove Gson dependency
- Update dependencies
- Compile sdk version is 31
- Fix: average speed

## Kotori 3.1.0

- Show location altitude
- Use GNSS API on devices running android 7 or above (Migrate to androidX in future release)
- Code clean up (refactor, deprecated APIs...)

## Kotori 3.0.2

Emergency release

- Fix: Application does not completly start after grant location permision
- Update gradle again

## Kotori 3.0.1

- Update gradle wrapper

## Kotori 3.0.0

First fork release

- Port to androidX
- Use Material Components
- Cosmetic changes (icons, colors, ui elements position)
- Night mode (android 10+)
